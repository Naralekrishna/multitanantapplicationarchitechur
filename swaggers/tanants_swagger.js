/**
 * @swagger
 * /v1/tanants?page_number={page_number}&page_count={page_count}&sort_on={sort_on}&sort_by={sort_by}:
 *  get:
 *      tags:
 *        - TANANTS
 *      summary: Resturns tanants
 *      description: List of tanants
 *      produces:
 *          application/json
 *      parameters:
 *       - name: page_number
 *         in: path
 *         required: true
 *         type: integer
 *         example: 0
 *       - name: page_count
 *         in: path
 *         required: true
 *         type: integer
 *         example: 10
 *       - name: sort_on
 *         in: path
 *         required: true
 *         type: string
 *         example: id
 *       - name: sort_by
 *         in: path
 *         required: true
 *         type: string
 *         example: desc
 *      responses:
 *          200:
 *              description: Success.
 *              content:
 *                  application/json
 *          500:
 *              description: Failed.
 */

/**
 * @swagger
 * definitions:
 *  Tanant:
 *      type: object
 *      required:
 *          - title
 *      properties:
 *          tanant_name:
 *              type: string
 *              required: true
 *              example: 'MCU'
 *          tanant_code:
 *              type: string
 *              required: true
 *              example: 'AA'
 *          first_name:
 *              type: string
 *              required: true
 *              example: 'John'
 *          last_name:
 *              type: string
 *              required: true
 *              example: 'D'
 *          email:
 *              type: string
 *              required: true
 *              example: 'John@email.com'
 *          password:
 *              type: string
 *              required: true
 *              example: 'John@123'
 */

/**
 * @swagger
 * /v1/tanants/:
 *  post:
 *      tags:
 *          - TANANTS
 *      summary: Create tanant
 *      description: Create new tanant
 *      consumes:
 *          - application/json
 *      parameters:
 *          - in: body
 *            name: request
 *            required: true
 *            example: Request body
 *            schema:
 *              $ref: '#/definitions/Tanant'
 *      responses:
 *          201:
 *              description: Created.
 *              content:
 *                  application/json
 *          500:
 *              description: Failed.
 */