var express = require('express');
var router = express.Router();

const tanantService = require(`../../services/tanantService`);

router.get('/', async (req, res, next) => {
  try {
    const tanants = await tanantService.tanants(req.query);
    res.status(tanants.status).json(tanants);
  } catch (error) {
    res.status(error.status).json({ status: error.status, message: error.message });
  }
});

router.post('/', async (req, res, next) => {
  try {
    const tanant = await tanantService.create(req.body);
    res.status(tanant.status).json(tanant);
  } catch (error) {
    res.status(error.status).json({ status: error.status, message: error.message });
  }
});

module.exports = router;
