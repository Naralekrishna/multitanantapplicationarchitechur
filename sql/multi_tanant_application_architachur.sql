--
-- PostgreSQL database dump
--

-- Dumped from database version 14.0
-- Dumped by pg_dump version 14.0

-- Started on 2021-12-17 15:54:09

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4 (class 2615 OID 16724)
-- Name: AA; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "AA";


ALTER SCHEMA "AA" OWNER TO postgres;

--
-- TOC entry 227 (class 1255 OID 16684)
-- Name: create_schema(character varying); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.create_schema(IN tanant_code character varying)
    LANGUAGE plpgsql
    AS $$
	begin
		EXECUTE 'CREATE SCHEMA ' || quote_ident(tanant_code);
		EXECUTE 'SET search_path TO ' || quote_ident(tanant_code);

		CREATE TABLE IF NOT EXISTS tags (id serial NOT NULL, name text NULL, CONSTRAINT tags_pkey PRIMARY KEY (id)); ALTER TABLE tags OWNER to postgres; INSERT INTO tags(name) VALUES ('In Progress'), ('Complete');
		commit;
	end;
	$$;


ALTER PROCEDURE public.create_schema(IN tanant_code character varying) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 215 (class 1259 OID 16726)
-- Name: tags; Type: TABLE; Schema: AA; Owner: postgres
--

CREATE TABLE "AA".tags (
    id integer NOT NULL,
    name text
);


ALTER TABLE "AA".tags OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 16725)
-- Name: tags_id_seq; Type: SEQUENCE; Schema: AA; Owner: postgres
--

CREATE SEQUENCE "AA".tags_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "AA".tags_id_seq OWNER TO postgres;

--
-- TOC entry 3340 (class 0 OID 0)
-- Dependencies: 214
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: AA; Owner: postgres
--

ALTER SEQUENCE "AA".tags_id_seq OWNED BY "AA".tags.id;


--
-- TOC entry 211 (class 1259 OID 16409)
-- Name: tanants; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tanants (
    id integer NOT NULL,
    tanant_name character varying(255) NOT NULL,
    tanant_code character varying(2) NOT NULL,
    created_at date,
    created_by character varying,
    updated_by character varying,
    updated_at date
);


ALTER TABLE public.tanants OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16408)
-- Name: tanants_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tanants_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tanants_id_seq OWNER TO postgres;

--
-- TOC entry 3341 (class 0 OID 0)
-- Dependencies: 210
-- Name: tanants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tanants_id_seq OWNED BY public.tanants.id;


--
-- TOC entry 213 (class 1259 OID 16432)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying,
    tanant_id integer DEFAULT 0 NOT NULL,
    created_at date,
    created_by character varying,
    updated_at date,
    updated_by character varying
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 16431)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- TOC entry 3342 (class 0 OID 0)
-- Dependencies: 212
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 3179 (class 2604 OID 16729)
-- Name: tags id; Type: DEFAULT; Schema: AA; Owner: postgres
--

ALTER TABLE ONLY "AA".tags ALTER COLUMN id SET DEFAULT nextval('"AA".tags_id_seq'::regclass);


--
-- TOC entry 3176 (class 2604 OID 16412)
-- Name: tanants id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tanants ALTER COLUMN id SET DEFAULT nextval('public.tanants_id_seq'::regclass);


--
-- TOC entry 3177 (class 2604 OID 16435)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 3334 (class 0 OID 16726)
-- Dependencies: 215
-- Data for Name: tags; Type: TABLE DATA; Schema: AA; Owner: postgres
--

COPY "AA".tags (id, name) FROM stdin;
1	In Progress
2	Complete
\.


--
-- TOC entry 3330 (class 0 OID 16409)
-- Dependencies: 211
-- Data for Name: tanants; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tanants (id, tanant_name, tanant_code, created_at, created_by, updated_by, updated_at) FROM stdin;
1	MCU	AA	2021-12-17	system	\N	\N
\.


--
-- TOC entry 3332 (class 0 OID 16432)
-- Dependencies: 213
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, first_name, last_name, email, password, tanant_id, created_at, created_by, updated_at, updated_by) FROM stdin;
1	John	D	John@email.com	John@123	1	2021-12-17	system	\N	\N
\.


--
-- TOC entry 3343 (class 0 OID 0)
-- Dependencies: 214
-- Name: tags_id_seq; Type: SEQUENCE SET; Schema: AA; Owner: postgres
--

SELECT pg_catalog.setval('"AA".tags_id_seq', 2, true);


--
-- TOC entry 3344 (class 0 OID 0)
-- Dependencies: 210
-- Name: tanants_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tanants_id_seq', 1, true);


--
-- TOC entry 3345 (class 0 OID 0)
-- Dependencies: 212
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 1, true);


--
-- TOC entry 3189 (class 2606 OID 16733)
-- Name: tags tags_pkey; Type: CONSTRAINT; Schema: AA; Owner: postgres
--

ALTER TABLE ONLY "AA".tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- TOC entry 3181 (class 2606 OID 16414)
-- Name: tanants tanants_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tanants
    ADD CONSTRAINT tanants_pk PRIMARY KEY (id);


--
-- TOC entry 3183 (class 2606 OID 16430)
-- Name: tanants tanants_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tanants
    ADD CONSTRAINT tanants_un UNIQUE (tanant_code);


--
-- TOC entry 3185 (class 2606 OID 16440)
-- Name: users users_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pk PRIMARY KEY (id);


--
-- TOC entry 3187 (class 2606 OID 16442)
-- Name: users users_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_un UNIQUE (email);


-- Completed on 2021-12-17 15:54:09

--
-- PostgreSQL database dump complete
--

