const pgpool = require(`../lib/pgpool`);

const tanants = async ({ page_number, page_count, sort_by, sort_on }) => {
    try {
        const tanants = await pgpool.query(`select t.id, t.tanant_name, t.tanant_code from public.tanants t order by t.${sort_on} ${sort_by} limit $1 offset $2`, [+page_count, (+page_number * +page_count)]);
        return { status: 200, message: (tanants.rowCount > 0) ? `data found` : `no records found`, data: tanants.rows };
    } catch (error) {
        throw { status: 400, message: error.message };
    }
}

const create = async ({ tanant_name, tanant_code, first_name, last_name, email, password }) => {
    try {
        const schema = await createSchema(tanant_code);
        if (schema) {
            const tanant = await pgpool.query(`insert into public.tanants (tanant_name, tanant_code, created_by, created_at, updated_by, updated_at) values ($1, $2, $3, now(), $4, $5) returning id, tanant_name`, [tanant_name, tanant_code, `system`, null, null]);

            const user = await pgpool.query(`insert into public.users (first_name, last_name, email, password, tanant_id, created_by, created_at, updated_by, updated_at) values ($1, $2, $3, $4, $5, $6, now(), $7, $8)`, [first_name, last_name, email, password, tanant.rows[0].id, `system`, null, null]);

            return { status: 201, message: `data inserted`, data: tanant.rows[0].tanant_name };
        } else {
            throw { message: `schema not created` };
        }
    } catch (error) {
        throw { status: 400, message: error.message };
    }
}

const createSchema = async (schema) => {
    try {
        const createdSchema = await pgpool.query(`CALL create_schema($1);`, [schema]);
        return true;
    } catch (error) {
        throw { status: 400, message: error.message };
    }
}

module.exports = { tanants, create };